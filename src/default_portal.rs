use crate::database::Database;
use crate::feed_api::portal::{Portal, PortalErrorKind, PortalResult};
use crate::models::{Article, ArticleFilter, ArticleID, Category, CategoryID, Feed, FeedID, FeedMapping, Headline, Tag};
use failure::ResultExt;
use std::sync::Arc;

pub struct DefaultPortal {
    db: Arc<Database>,
}

unsafe impl Send for DefaultPortal {}
unsafe impl Sync for DefaultPortal {}

impl DefaultPortal {
    pub fn new(db: Arc<Database>) -> DefaultPortal {
        DefaultPortal { db }
    }
}

impl Portal for DefaultPortal {
    fn get_headlines(&self, ids: &[ArticleID]) -> PortalResult<Vec<Headline>> {
        let articles = self
            .db
            .read_articles(ArticleFilter {
                limit: None,
                offset: Some(ids.len() as i64),
                order: None,
                unread: None,
                marked: None,
                feed: None,
                feed_blacklist: None,
                category: None,
                category_blacklist: None,
                tag: None,
                ids: Some(ids),
                newer_than: None,
                older_than: None,
                search_term: None,
            })
            .context(PortalErrorKind::DB)?;
        let headlines = articles.iter().map(|a| Headline::from_article(a)).collect();
        Ok(headlines)
    }

    fn get_articles(&self, ids: &[ArticleID]) -> PortalResult<Vec<Article>> {
        let articles = self
            .db
            .read_articles(ArticleFilter {
                limit: Some(ids.len() as i64),
                offset: None,
                order: None,
                unread: None,
                marked: None,
                feed: None,
                feed_blacklist: None,
                category: None,
                category_blacklist: None,
                tag: None,
                ids: Some(ids),
                newer_than: None,
                older_than: None,
                search_term: None,
            })
            .context(PortalErrorKind::DB)?;
        Ok(articles)
    }

    fn get_article_exists(&self, id: &ArticleID) -> PortalResult<bool> {
        let exists = self.db.article_exists(id).context(PortalErrorKind::DB)?;
        Ok(exists)
    }

    fn get_article_ids_unread_feed(&self, feed_id: &FeedID) -> PortalResult<Vec<ArticleID>> {
        let articles = self.db.read_articles(ArticleFilter::feed_unread(feed_id)).context(PortalErrorKind::DB)?;

        Ok(articles.iter().map(|article| article.article_id.clone()).collect())
    }

    fn get_article_ids_unread_category(&self, category_id: &CategoryID) -> PortalResult<Vec<ArticleID>> {
        let articles = self
            .db
            .read_articles(ArticleFilter::category_unread(category_id))
            .context(PortalErrorKind::DB)?;

        Ok(articles.iter().map(|article| article.article_id.clone()).collect())
    }

    fn get_article_ids_unread_all(&self) -> PortalResult<Vec<ArticleID>> {
        let articles = self.db.read_articles(ArticleFilter::all_unread()).context(PortalErrorKind::DB)?;

        Ok(articles.iter().map(|article| article.article_id.clone()).collect())
    }

    fn get_article_ids_marked_all(&self) -> PortalResult<Vec<ArticleID>> {
        let articles = self.db.read_articles(ArticleFilter::all_marked()).context(PortalErrorKind::DB)?;

        Ok(articles.iter().map(|article| article.article_id.clone()).collect())
    }

    fn get_feeds(&self) -> PortalResult<Vec<Feed>> {
        let feeds = self.db.read_feeds().context(PortalErrorKind::DB)?;
        Ok(feeds)
    }

    fn get_categories(&self) -> PortalResult<Vec<Category>> {
        let categories = self.db.read_categories().context(PortalErrorKind::DB)?;
        Ok(categories)
    }

    fn get_mappings(&self) -> PortalResult<Vec<FeedMapping>> {
        let mappings = self.db.read_mappings(None, None).context(PortalErrorKind::DB)?;
        Ok(mappings)
    }

    fn get_tags(&self) -> PortalResult<Vec<Tag>> {
        let tags = self.db.read_tags().context(PortalErrorKind::DB)?;
        Ok(tags)
    }
}
