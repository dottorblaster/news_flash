use crate::feed_api::Portal;
use crate::models::{
    ArticleID, Category, CategoryID, CategoryType, Direction, FatArticle, Feed, FeedID, FeedMapping, Headline, Marked, Read, Tag, TagID, Tagging,
    Url, NEWSFLASH_TOPLEVEL,
};
use chrono::{NaiveDateTime, Utc};
use greader_api::error::ApiError;
use greader_api::models::{Category as GCategory, Feed as GFeed, Item, ItemId, ItemRefs, Stream, Summary, Tagging as GTagging, Taggings};
use greader_api::GReaderApi;
use parking_lot::RwLock;
use rayon::prelude::*;
use reqwest::Client;
use std::collections::HashSet;
use std::convert::TryInto;

use super::html2text::Html2Text;

pub const TAG_READ_STR: &str = "user/-/state/com.google/read";
pub const TAG_STARRED_STR: &str = "user/-/state/com.google/starred";

pub struct GReaderUtil;

impl GReaderUtil {
    pub fn generate_tag_id(name: &str) -> String {
        format!("user/-/label/{}", name)
    }

    pub fn convert_category(category: GCategory, sort_index: Option<i32>) -> Category {
        let (id, title) = category.decompose();
        Category {
            category_id: CategoryID::new(&id),
            label: title,
            sort_index,
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            category_type: CategoryType::Default,
        }
    }

    pub fn convert_category_vec(mut categories: Vec<GFeed>) -> Vec<Category> {
        // skip duplicate categories by collecting all ids
        let mut category_ids: HashSet<CategoryID> = HashSet::new();

        // normalize index by only issuing a new index when a new unique category is found
        let mut index = 0;

        categories
            .drain(..)
            .filter_map(|feed| {
                let feed_categories: Vec<Category> = feed
                    .categories
                    .into_iter()
                    .filter_map(|c| {
                        let category = Self::convert_category(c, Some(index));
                        if category_ids.contains(&category.category_id) {
                            None
                        } else {
                            index += 1;
                            category_ids.insert(category.category_id.clone());
                            Some(category)
                        }
                    })
                    .collect();

                if feed_categories.is_empty() {
                    None
                } else {
                    Some(feed_categories)
                }
            })
            .flatten()
            .collect()
    }

    pub fn convert_tag_list(taggings: Taggings, categories: &[Category]) -> Vec<Tag> {
        let Taggings { tags: tag_list } = taggings;

        tag_list
            .into_iter()
            .filter_map(|tagging| {
                let GTagging {
                    id: tag_id,
                    r#type: _,
                    sortid,
                    unread_count: _,
                    unseen_count: _,
                } = tagging;

                // tags and folder both have the structure 'user/-/label/$NAME'
                if tag_id.contains("/label/") {
                    // if 'tag_id' is a category ignore it
                    if categories.iter().any(|c| c.category_id.to_str() == tag_id) {
                        None
                    } else {
                        let label = tag_id.split('/').last().map(|s| s.to_owned()).unwrap_or("Missing Label".into());

                        let sort_index = sortid
                            .map(|str| hex::decode(str).ok())
                            .flatten()
                            .map(|buf| buf.try_into().ok())
                            .flatten()
                            .map(|buf| u32::from_le_bytes(buf) as i32);

                        Some(Tag {
                            tag_id: TagID::from_owned(tag_id),
                            label,
                            color: None,
                            sort_index,
                        })
                    }
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn convert_feed(feed: GFeed, sort_index: Option<i32>) -> Feed {
        let (id, title, _categories, _url, html_url, _icon_url) = feed.decompose();

        Feed {
            feed_id: FeedID::new(&id),
            label: title,
            website: match Url::parse(&html_url) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            feed_url: match Url::parse(&html_url) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            icon_url: None,
            sort_index,
        }
    }

    pub fn convert_feed_vec(mut feeds: Vec<GFeed>) -> (Vec<Feed>, Vec<FeedMapping>) {
        let mut mappings: Vec<FeedMapping> = Vec::new();
        let feeds = feeds
            .drain(..)
            .enumerate()
            .map(|(i, f)| {
                for category in &f.categories {
                    mappings.push(FeedMapping {
                        feed_id: FeedID::new(&f.id.to_string()),
                        category_id: CategoryID::new(&category.id.to_string()),
                    });
                }

                Self::convert_feed(f, Some(i as i32))
            })
            .collect();

        (feeds, mappings)
    }

    pub fn convert_item_vec(articles: Vec<Item>, tags: &[Tag], portal: &Box<dyn Portal>) -> (Vec<FatArticle>, Vec<Headline>, Vec<Tagging>) {
        let headlines: RwLock<Vec<Headline>> = RwLock::new(Vec::new());
        let taggings: RwLock<Vec<Tagging>> = RwLock::new(Vec::new());

        let articles = articles
            .into_par_iter()
            .filter_map(|item| {
                let (origin, _updated, id, categories, author, alternate, _timestamp_usec, _crawl_time_msec, published, title, content) =
                    item.decompose();

                let article_id = ArticleID::new(&id);
                let article_exists_locally = portal.get_article_exists(&article_id).unwrap_or(false);

                let unread = if categories.iter().any(|c| c.ends_with("/read")) {
                    Read::Read
                } else {
                    Read::Unread
                };
                let marked = if categories.iter().any(|c| c.ends_with("/starred")) {
                    Marked::Marked
                } else {
                    Marked::Unmarked
                };

                let mut article_taggings = categories
                    .iter()
                    .filter_map(|c| {
                        if tags.iter().any(|tag| tag.tag_id.to_str() == c) {
                            Some(Tagging {
                                tag_id: TagID::new(c),
                                article_id: article_id.clone(),
                            })
                        } else {
                            None
                        }
                    })
                    .collect();

                taggings.write().append(&mut article_taggings);

                // already in db
                // -> only need to update read/marked status
                // FIXME: can we check if article was recrawled and updated its content?
                if article_exists_locally {
                    headlines.write().push(Headline { article_id, unread, marked });
                    return None;
                }

                let url = alternate.first().map(|alt| Url::parse(&alt.href).ok()).flatten();
                let (html, direction) = if let Some(content) = content {
                    let Summary { content: html, direction } = content;
                    let direction = direction.map(|d| if d == "rtl" { Direction::RightToLeft } else { Direction::LeftToRight });
                    (Some(html), direction)
                } else {
                    (None, None)
                };

                let plain_text = if article_exists_locally {
                    None
                } else if let Some(html) = &html {
                    Html2Text::process(html)
                } else {
                    None
                };

                let summary = plain_text.as_ref().map(|plain_text| Html2Text::to_summary(plain_text));

                Some(FatArticle {
                    article_id,
                    title: title
                        .map(|t| match escaper::decode_html(&t) {
                            Ok(title) => Some(title),
                            Err(_error) => {
                                // This warning freaks users out for some reason
                                // warn!("Error {:?} at character {}", error.kind, error.position);
                                Some(t)
                            }
                        })
                        .flatten(),
                    author,
                    feed_id: FeedID::new(&origin.stream_id),
                    url,
                    date: NaiveDateTime::from_timestamp(published, 0),
                    synced: Utc::now().naive_utc(),
                    html,
                    direction,
                    summary,
                    plain_text,
                    scraped_content: None,
                    unread,
                    marked,
                    thumbnail_url: None,
                })
            })
            .collect();

        (articles, headlines.into_inner(), taggings.into_inner())
    }

    pub async fn get_articles(
        api: &GReaderApi,
        client: &Client,
        feed_id: Option<FeedID>,
        read: Option<Read>,
        marked: Option<Marked>,
        tags: &[Tag],
        limit: Option<u64>,
        portal: &Box<dyn Portal>,
    ) -> Result<(Vec<FatArticle>, Vec<Headline>, Vec<Tagging>), ApiError> {
        let mut continuation: Option<String> = None;
        let mut articles = Vec::new();
        let mut headlines = Vec::new();
        let mut taggings = Vec::new();
        let stream_id = feed_id.map(|id| id.to_string());
        let exclude = read.map(|r| if r == Read::Unread { Some(TAG_READ_STR) } else { None }).flatten();
        let include = read.map(|r| if r == Read::Read { Some(TAG_READ_STR) } else { None }).flatten();
        let include = marked
            .map(|m| {
                if m == Marked::Marked {
                    Some(include.unwrap_or(TAG_STARRED_STR))
                } else {
                    None
                }
            })
            .flatten();

        let amount = limit.map(|l| u64::max(l, 1000)).unwrap_or(1000);
        let mut missing = limit.map(|l| if l < 1000 { 0 } else { l - 1000 }).unwrap_or(0);

        loop {
            let stream = api
                .stream_contents(
                    stream_id.as_deref(),
                    false,
                    Some(amount),
                    continuation.as_deref(),
                    exclude,
                    include,
                    None,
                    None,
                    client,
                )
                .await?;

            let Stream {
                direction: _,
                id: _,
                title: _,
                description: _,
                own: _,
                updated: _,
                updated_usec: _,
                items,
                author: _,
                continuation: _,
            } = stream;

            let (mut a, mut h, mut t) = Self::convert_item_vec(items, tags, portal);
            articles.append(&mut a);
            headlines.append(&mut h);
            taggings.append(&mut t);

            if stream.continuation.is_none() {
                break;
            }

            if missing == 0 {
                break;
            }

            missing -= amount;
            continuation = stream.continuation.clone();
        }

        Ok((articles, headlines, taggings))
    }

    pub async fn get_article_ids(
        api: &GReaderApi,
        client: &Client,
        feed_id: Option<FeedID>,
        read: Option<Read>,
        marked: Option<Marked>,
    ) -> Result<Vec<ItemId>, ApiError> {
        let mut continuation: Option<String> = None;
        let mut article_ids = Vec::new();
        let stream_id = feed_id.map(|id| id.to_string());
        let exclude = read.map(|r| if r == Read::Unread { Some(TAG_READ_STR) } else { None }).flatten();
        let include = read.map(|r| if r == Read::Read { Some(TAG_READ_STR) } else { None }).flatten();
        let include = marked
            .map(|m| {
                if m == Marked::Marked {
                    Some(include.unwrap_or(TAG_STARRED_STR))
                } else {
                    None
                }
            })
            .flatten();

        loop {
            let stream = api
                .items_ids(
                    stream_id.as_deref(),
                    Some(1000),
                    false,
                    continuation.as_deref(),
                    exclude,
                    include,
                    None,
                    None,
                    client,
                )
                .await?;

            let ItemRefs { item_refs, continuation: c } = stream;
            if let Some(mut item_refs) = item_refs {
                article_ids.append(&mut item_refs);
            }

            if c.is_none() {
                break;
            }

            continuation = c.clone();
        }

        Ok(article_ids)
    }

    pub async fn get_article_contents(
        api: &GReaderApi,
        client: &Client,
        article_ids: &HashSet<ArticleID>,
        tags: &[Tag],
        portal: &Box<dyn Portal>,
    ) -> Result<(Vec<FatArticle>, Vec<Tagging>), ApiError> {
        let item_ids = article_ids.iter().map(|id| id.to_str().to_owned()).collect();
        let stream = api.items_contents(item_ids, client).await?;

        let Stream {
            direction: _,
            id: _,
            title: _,
            description: _,
            own: _,
            updated: _,
            updated_usec: _,
            items,
            author: _,
            continuation: _,
        } = stream;

        let (articles, _headlines, taggings) = Self::convert_item_vec(items, tags, portal);

        Ok((articles, taggings))
    }
}
