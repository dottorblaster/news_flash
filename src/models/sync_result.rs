use crate::models::{Category, Enclosure, FatArticle, Feed, FeedMapping, Headline, Tag, Tagging};
use chrono::{Duration, Utc};

use super::{Marked, Read};

pub struct SyncResult {
    pub feeds: Option<Vec<Feed>>,
    pub categories: Option<Vec<Category>>,
    pub mappings: Option<Vec<FeedMapping>>,
    pub tags: Option<Vec<Tag>>,
    pub headlines: Option<Vec<Headline>>,
    pub articles: Option<Vec<FatArticle>>,
    pub enclosures: Option<Vec<Enclosure>>,
    pub taggings: Option<Vec<Tagging>>,
}

impl SyncResult {
    pub fn remove_old_articles(self, older_than: Option<Duration>) -> Self {
        if let Some(older_than) = older_than {
            SyncResult {
                feeds: self.feeds,
                categories: self.categories,
                mappings: self.mappings,
                tags: self.tags,
                headlines: self.headlines,
                articles: self.articles.map(|articles| {
                    articles
                        .into_iter()
                        .filter(|a| a.synced >= Utc::now().naive_utc() - older_than || a.unread == Read::Unread || a.marked == Marked::Marked)
                        .collect()
                }),
                enclosures: self.enclosures,
                taggings: self.taggings,
            }
        } else {
            self
        }
    }
}
