use crate::models::{ArticleID, ArticleOrder, CategoryID, FeedID, Marked, Read, TagID};
use chrono::{DateTime, Utc};

#[derive(Clone, Debug)]
pub struct ArticleFilter<'a> {
    pub limit: Option<i64>,
    pub offset: Option<i64>,
    pub order: Option<ArticleOrder>,
    pub unread: Option<Read>,
    pub marked: Option<Marked>,
    pub feed: Option<FeedID>,
    pub feed_blacklist: Option<Vec<FeedID>>,
    pub category: Option<CategoryID>,
    pub category_blacklist: Option<Vec<CategoryID>>,
    pub tag: Option<TagID>,
    pub ids: Option<&'a [ArticleID]>,
    pub newer_than: Option<DateTime<Utc>>,
    pub older_than: Option<DateTime<Utc>>,
    pub search_term: Option<String>,
}

impl<'a> Default for ArticleFilter<'a> {
    fn default() -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: None,
            marked: None,
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: None,
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }
}

impl<'a> ArticleFilter<'a> {
    pub fn ids(article_ids: &'a [ArticleID]) -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: None,
            marked: None,
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: Some(article_ids),
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn read_ids(article_ids: &'a [ArticleID], read: Read) -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: Some(read),
            marked: None,
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: Some(article_ids),
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn marked_ids(article_ids: &'a [ArticleID], marked: Marked) -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: None,
            marked: Some(marked),
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: Some(article_ids),
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn feed_unread(feed_id: &FeedID) -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: Some(Read::Unread),
            marked: None,
            feed: Some(feed_id.clone()),
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: None,
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn category_unread(category_id: &CategoryID) -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: Some(Read::Unread),
            marked: None,
            feed: None,
            feed_blacklist: None,
            category: Some(category_id.clone()),
            category_blacklist: None,
            tag: None,
            ids: None,
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn tag_unread(tag_id: &TagID) -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: Some(Read::Unread),
            marked: None,
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: Some(tag_id.clone()),
            ids: None,
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn all_unread() -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: Some(Read::Unread),
            marked: None,
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: None,
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }

    pub fn all_marked() -> Self {
        Self {
            limit: None,
            offset: None,
            order: None,
            unread: None,
            marked: Some(Marked::Marked),
            feed: None,
            feed_blacklist: None,
            category: None,
            category_blacklist: None,
            tag: None,
            ids: None,
            newer_than: None,
            older_than: None,
            search_term: None,
        }
    }
}
