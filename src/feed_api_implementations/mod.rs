pub mod feedbin;
pub mod feedly;
pub mod fever;
pub mod freshrss;
pub mod inoreader;
pub mod local;
pub mod miniflux;
pub mod newsblur;
pub mod nextcloud;

use self::feedbin::metadata::FeedbinMetadata;
use self::feedly::{feedly_secrets::FeedlySecrets, metadata::FeedlyMetadata};
use self::fever::metadata::FeverMetadata;
use self::freshrss::metadata::FreshRssMetadata;
use self::inoreader::metadata::InoreaderMetadata;
use self::local::metadata::LocalMetadata;
use self::miniflux::metadata::MinifluxMetadata;
use self::newsblur::metadata::NewsBlurMetadata;
use self::nextcloud::metadata::NextcloudMetadata;
use crate::feed_api::ApiMetadata;
use crate::models::PluginID;

pub struct FeedApiImplementations;

impl FeedApiImplementations {
    pub fn list() -> Vec<Box<dyn ApiMetadata>> {
        let mut h: Vec<Box<dyn ApiMetadata>> = vec![
            Box::new(FeedbinMetadata),
            Box::new(FeverMetadata),
            Box::new(FreshRssMetadata),
            Box::new(MinifluxMetadata),
            Box::new(NewsBlurMetadata),
            Box::new(LocalMetadata),
            Box::new(InoreaderMetadata),
            Box::new(NextcloudMetadata),
        ];

        // only offer feedly when API secrets are available
        if FeedlySecrets::new().valid() {
            h.push(Box::new(FeedlyMetadata));
        }

        h
    }

    pub fn get(id: &PluginID) -> Option<Box<dyn ApiMetadata>> {
        let list = Self::list();
        for api_meta in list {
            if &api_meta.id() == id {
                return Some(api_meta);
            }
        }

        None
    }
}
