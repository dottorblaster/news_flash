use super::config::AccountConfig;
use super::Nextcloud;
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{LoginGUI, PasswordLoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, Url, VectorIcon};
use failure::{Fail, ResultExt};
use nextcloud_news_api::NextcloudNewsApi;
use rust_embed::RustEmbed;
use std::path::PathBuf;
use std::str;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/nextcloud/icons"]
struct NextcloudResources;

pub struct NextcloudMetadata;

impl NextcloudMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("nextcloud")
    }
}

impl ApiMetadata for NextcloudMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = NextcloudResources::get("feed-service-nextcloud.svg").ok_or(FeedApiErrorKind::Resource)?;
        let icon = VectorIcon {
            data: icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = NextcloudResources::get("feed-service-nextcloud-symbolic.svg").ok_or(FeedApiErrorKind::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.data.to_vec(),
            width: 48,
            height: 24,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::Password(PasswordLoginGUI { url: true, http_auth: false });

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("Nextcloud News"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: match Url::parse("https://apps.nextcloud.com/apps/news") {
                Ok(website) => Some(website),
                Err(_) => None,
            },
            service_type: ServiceType::Remote { self_hosted: true },
            license_type: ServiceLicense::GPLv3,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn parse_error(&self, _error: &dyn Fail) -> Option<String> {
        None
    }

    fn get_instance(&self, path: &PathBuf, portal: Box<dyn Portal>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path).context(FeedApiErrorKind::Config)?;
        let mut api: Option<NextcloudNewsApi> = None;

        if let Some(url) = account_config.get_url() {
            if let Ok(url) = Url::parse(&url) {
                if let Some(username) = account_config.get_user_name() {
                    if let Some(password) = account_config.get_password() {
                        api = Some(NextcloudNewsApi::new(&url, username, password));
                    }
                }
            }
        }

        let logged_in = api.is_some();

        let nextcloud = Nextcloud {
            api,
            portal,
            logged_in,
            config: account_config,
        };
        let nextcloud = Box::new(nextcloud);
        Ok(nextcloud)
    }
}
