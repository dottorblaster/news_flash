mod config;
pub mod metadata;

use std::collections::HashSet;

use self::config::AccountConfig;
use self::metadata::NextcloudMetadata;
use crate::feed_api::{FeedApi, FeedApiError, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{
    self, ArticleID, Category, CategoryID, CategoryType, Direction, Enclosure, FatArticle, FavIcon, Feed, FeedID, FeedMapping, LoginData, Marked,
    PasswordLogin, PluginCapabilities, Read, SyncResult, TagID, Url, NEWSFLASH_TOPLEVEL,
};
use crate::util;
use crate::util::html2text::Html2Text;
use async_trait::async_trait;
use chrono::{DateTime, NaiveDateTime, Utc};
use failure::ResultExt;
use log::error;
use nextcloud_news_api::models::{Feed as NcFeed, Folder, Item, ItemType};
use nextcloud_news_api::NextcloudNewsApi;
use parking_lot::RwLock;
use rayon::prelude::*;
use reqwest::Client;
use semver::Version;

pub struct Nextcloud {
    api: Option<NextcloudNewsApi>,
    portal: Box<dyn Portal>,
    logged_in: bool,
    config: AccountConfig,
}

impl Nextcloud {
    fn ids_to_nc_ids<T: ToString>(ids: &[T]) -> Vec<i64> {
        ids.iter().filter_map(|id| id.to_string().parse::<i64>().ok()).collect()
    }

    fn id_to_nc_id<T: ToString>(id: &T) -> Option<i64> {
        id.to_string().parse::<i64>().ok()
    }

    fn convert_folder_vec(mut categories: Vec<Folder>) -> Vec<Category> {
        categories
            .drain(..)
            .enumerate()
            .map(|(i, c)| Self::convert_folder(c, Some(i as i32)))
            .collect()
    }

    fn convert_folder(folder: Folder, sort_index: Option<i32>) -> Category {
        let Folder { id, name } = folder;
        Category {
            category_id: CategoryID::new(&id.to_string()),
            label: name,
            sort_index,
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            category_type: CategoryType::Default,
        }
    }

    fn convert_feed(feed: NcFeed, sort_index: Option<i32>) -> Feed {
        let NcFeed {
            id,
            url,
            title,
            favicon_link,
            added: _,
            folder_id: _,
            unread_count: _,
            ordering: _,
            link,
            pinned: _,
            update_error_count: _,
            last_update_error: _,
        } = feed;

        Feed {
            feed_id: FeedID::new(&id.to_string()),
            label: title,
            website: link.map(|link| Url::parse(&link).ok()).flatten(),
            feed_url: match Url::parse(&url) {
                Ok(url) => Some(url),
                Err(_) => None,
            },
            icon_url: favicon_link.map(|url| Url::parse(&url).ok()).flatten(),
            sort_index,
        }
    }

    fn convert_feed_vec(mut feeds: Vec<NcFeed>) -> (Vec<Feed>, Vec<FeedMapping>) {
        let mut mappings: Vec<FeedMapping> = Vec::new();
        let feeds = feeds
            .drain(..)
            .enumerate()
            .map(|(i, f)| {
                if let Some(folder_id) = f.folder_id {
                    mappings.push(FeedMapping {
                        feed_id: FeedID::new(&f.id.to_string()),
                        category_id: CategoryID::new(&folder_id.to_string()),
                    });
                }

                Self::convert_feed(f, Some(i as i32))
            })
            .collect();

        (feeds, mappings)
    }

    fn convert_item_vec(items: Vec<Item>, feed_ids: &HashSet<FeedID>, portal: &Box<dyn Portal>) -> (Vec<FatArticle>, Vec<Enclosure>) {
        let enclosures: RwLock<Vec<Enclosure>> = RwLock::new(Vec::new());
        let articles = items
            .into_par_iter()
            .filter_map(|i| {
                if feed_ids.contains(&FeedID::new(&i.feed_id.to_string())) || i.starred {
                    let (article, enclousre) = Self::convert_item(i, portal);
                    if let Some(enclosure) = enclousre {
                        enclosures.write().push(enclosure);
                    }
                    Some(article)
                } else {
                    None
                }
            })
            .collect();

        (articles, enclosures.into_inner())
    }

    fn convert_item(item: Item, portal: &Box<dyn Portal>) -> (FatArticle, Option<Enclosure>) {
        let Item {
            id,
            guid: _,
            guid_hash: _,
            url,
            title,
            author,
            pub_date,
            body,
            enclosure_mime,
            enclosure_link,
            media_thumbnail,
            media_description: _,
            feed_id,
            unread,
            starred,
            rtl,
            last_modified: _,
            fingerprint: _,
        } = item;

        let article_id = ArticleID::new(&id.to_string());

        let article_exists_locally = portal.get_article_exists(&article_id).unwrap_or(false);

        let plain_text = if article_exists_locally { None } else { Html2Text::process(&body) };

        let summary = plain_text.as_ref().map(|plain_text| Html2Text::to_summary(plain_text));

        let article = FatArticle {
            article_id: article_id.clone(),
            title,
            author,
            feed_id: FeedID::new(&feed_id.to_string()),
            url: url.map(|url| Url::parse(&url).ok()).flatten(),
            date: NaiveDateTime::from_timestamp(pub_date as i64, 0),
            synced: Utc::now().naive_utc(),
            summary,
            html: Some(body),
            direction: Some(if rtl { Direction::RightToLeft } else { Direction::LeftToRight }),
            unread: if unread { Read::Unread } else { Read::Read },
            marked: if starred { Marked::Marked } else { Marked::Unmarked },
            scraped_content: None,
            plain_text,
            thumbnail_url: media_thumbnail,
        };
        let enclosure = enclosure_link
            .map(|enc_url| {
                Url::parse(&enc_url).ok().map(|url| Enclosure {
                    article_id,
                    url,
                    mime_type: enclosure_mime,
                    title: None,
                })
            })
            .flatten();

        (article, enclosure)
    }
}

#[async_trait]
impl FeedApi for Nextcloud {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::ADD_REMOVE_FEEDS | PluginCapabilities::SUPPORT_CATEGORIES | PluginCapabilities::MODIFY_CATEGORIES)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(self.api.is_some())
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        Ok(self.logged_in)
    }

    fn user_name(&self) -> Option<String> {
        self.config.get_user_name()
    }

    fn get_login_data(&self) -> Option<LoginData> {
        if let Ok(true) = self.has_user_configured() {
            if let Some(username) = self.config.get_user_name() {
                if let Some(password) = self.config.get_password() {
                    return Some(LoginData::Password(PasswordLogin {
                        id: NextcloudMetadata::get_id(),
                        url: self.config.get_url(),
                        user: username,
                        password,
                        http_user: None,
                        http_password: None,
                    }));
                }
            }
        }

        None
    }

    async fn login(&mut self, data: LoginData, client: &Client) -> FeedApiResult<()> {
        if let LoginData::Password(data) = data {
            let url_string = data.url.clone().ok_or(FeedApiErrorKind::Url)?;
            let url = Url::parse(&url_string).context(FeedApiErrorKind::Url)?;
            let api = NextcloudNewsApi::new(&url, data.user.clone(), data.password.clone());

            let nextcloud_news_api::models::Version { version: version_string } = api.get_version(client).await.context(FeedApiErrorKind::Api)?;
            let semver = Version::parse(&version_string).context(FeedApiErrorKind::Api)?;
            let min_version = Version::new(18, 1, 1);
            if semver < min_version {
                error!("Nextcloud News app is version {}. Minimal required version is {}.", semver, min_version);
                return Err(FeedApiErrorKind::UnsupportedVersion.into());
            }

            self.config.set_url(&url_string);
            self.config.set_password(&data.password);
            self.config.set_user_name(&data.user);
            self.config.write()?;
            self.api = Some(api);
            self.logged_in = true;
            return Ok(());
        }

        self.logged_in = false;
        self.api = None;
        Err(FeedApiErrorKind::Login.into())
    }

    async fn logout(&mut self, _client: &Client) -> FeedApiResult<()> {
        self.config.delete()?;
        Ok(())
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let folders = api.get_folders(client).await.context(FeedApiErrorKind::Api)?;
            let categories = Nextcloud::convert_folder_vec(folders);

            let feeds = api.get_feeds(client).await.context(FeedApiErrorKind::Api)?;
            let (feeds, mappings) = Nextcloud::convert_feed_vec(feeds);

            let feed_id_set: HashSet<FeedID> = feeds.iter().map(|f| f.feed_id.clone()).collect();

            let mut articles: Vec<FatArticle> = Vec::new();
            let mut enclosures: Vec<Enclosure> = Vec::new();

            let unread_items = api
                .get_items(client, -1, None, None, None, Some(false), None)
                .await
                .context(FeedApiErrorKind::Api)?;
            let (mut unread_articles, mut unread_enclousres) = Self::convert_item_vec(unread_items, &feed_id_set, &self.portal);

            articles.append(&mut unread_articles);
            enclosures.append(&mut unread_enclousres);

            let starred_items = api
                .get_items(client, -1, None, Some(ItemType::Starred), None, None, None)
                .await
                .context(FeedApiErrorKind::Api)?;
            let (mut starred_articles, mut starred_enclosures) = Self::convert_item_vec(starred_items, &feed_id_set, &self.portal);

            articles.append(&mut starred_articles);
            enclosures.append(&mut starred_enclosures);

            return Ok(SyncResult {
                feeds: util::vec_to_option(feeds),
                categories: util::vec_to_option(categories),
                mappings: util::vec_to_option(mappings),
                tags: None,
                taggings: None,
                headlines: None,
                articles: util::vec_to_option(articles),
                enclosures: util::vec_to_option(enclosures),
            });
        }

        Err(FeedApiErrorKind::Login.into())
    }

    async fn sync(&self, _max_count: u32, last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let folders = api.get_folders(client).await.context(FeedApiErrorKind::Api)?;
            let categories = Nextcloud::convert_folder_vec(folders);

            let feeds = api.get_feeds(client).await.context(FeedApiErrorKind::Api)?;
            let (feeds, mappings) = Nextcloud::convert_feed_vec(feeds);

            let feed_id_set: HashSet<FeedID> = feeds.iter().map(|f| f.feed_id.clone()).collect();

            let updated_items = api
                .get_updated_items(client, last_sync.timestamp() as u64, None, None)
                .await
                .context(FeedApiErrorKind::Api)?;
            let (articles, enclosures) = Self::convert_item_vec(updated_items, &feed_id_set, &self.portal);

            return Ok(SyncResult {
                feeds: util::vec_to_option(feeds),
                categories: util::vec_to_option(categories),
                mappings: util::vec_to_option(mappings),
                tags: None,
                taggings: None,
                headlines: None,
                articles: util::vec_to_option(articles),
                enclosures: util::vec_to_option(enclosures),
            });
        }

        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_article_read(&self, articles: &[ArticleID], read: models::Read, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let nc_ids = Self::ids_to_nc_ids(articles);

            match read {
                Read::Read => api.mark_items_read(client, nc_ids).await.context(FeedApiErrorKind::Api)?,
                Read::Unread => api.mark_items_unread(client, nc_ids).await.context(FeedApiErrorKind::Api)?,
            }

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_article_marked(&self, articles: &[ArticleID], marked: models::Marked, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let nc_ids = Self::ids_to_nc_ids(articles);

            match marked {
                Marked::Marked => api.mark_items_starred(client, nc_ids).await.context(FeedApiErrorKind::Api)?,
                Marked::Unmarked => api.mark_items_unstarred(client, nc_ids).await.context(FeedApiErrorKind::Api)?,
            }

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_feed_read(&self, feeds: &[FeedID], articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let nc_ids = Self::ids_to_nc_ids(feeds);
            let mut futures = Vec::new();
            for feed_id in nc_ids {
                let newest_unread_article_id = articles.first().map(Self::id_to_nc_id).flatten().unwrap_or(i64::MAX);
                futures.push(api.mark_feed(client, feed_id, newest_unread_article_id));
            }
            let results = futures::future::join_all(futures).await;
            let result: Result<Vec<()>, FeedApiError> = results
                .into_iter()
                .map(|res| res.context(FeedApiErrorKind::Api).map_err(FeedApiError::from))
                .collect();
            let _ = result?;

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_category_read(
        &self,
        categories: &[CategoryID],
        articles: &[ArticleID],
        _last_sync: DateTime<Utc>,
        client: &Client,
    ) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let nc_ids = Self::ids_to_nc_ids(categories);
            let mut futures = Vec::new();
            for folder_id in nc_ids {
                let newest_unread_article_id = articles.first().map(Self::id_to_nc_id).flatten().unwrap_or(i64::MAX);
                futures.push(api.mark_folder(client, folder_id, newest_unread_article_id));
            }
            let results = futures::future::join_all(futures).await;
            let result: Result<Vec<()>, FeedApiError> = results
                .into_iter()
                .map(|res| res.context(FeedApiErrorKind::Api).map_err(FeedApiError::from))
                .collect();
            let _ = result?;

            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_tag_read(&self, _tags: &[TagID], _articles: &[ArticleID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn set_all_read(&self, articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let newest_unread_article_id = articles.first().map(Self::id_to_nc_id).flatten().unwrap_or(i64::MAX);
            let _ = api
                .mark_all_items_read(client, newest_unread_article_id)
                .await
                .context(FeedApiErrorKind::Api)?;
            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
        if let Some(api) = &self.api {
            let folder_id = category_id.map(|id| Self::id_to_nc_id(&id)).flatten();

            let feed = api.create_feed(client, url.as_str(), folder_id).await.context(FeedApiErrorKind::Api)?;

            if let Some(title) = title {
                api.rename_feed(client, feed.id, &title).await.context(FeedApiErrorKind::Api)?;
            }

            let category = api
                .get_folders(client)
                .await
                .context(FeedApiErrorKind::Api)?
                .iter()
                .find(|f| Some(f.id) == folder_id)
                .map(|f| Self::convert_folder(f.clone(), None));

            return Ok((Self::convert_feed(feed, None), category));
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn remove_feed(&self, id: &FeedID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api.delete_feed(client, Self::id_to_nc_id(id).unwrap())
                .await
                .context(FeedApiErrorKind::Api)?;
            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn move_feed(&self, feed_id: &FeedID, _from: &CategoryID, to: &CategoryID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api.move_feed(client, Self::id_to_nc_id(feed_id).unwrap(), Some(Self::id_to_nc_id(to).unwrap()))
                .await
                .context(FeedApiErrorKind::Api)?;
            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn rename_feed(&self, feed_id: &FeedID, new_title: &str, client: &Client) -> FeedApiResult<FeedID> {
        if let Some(api) = &self.api {
            api.rename_feed(client, Self::id_to_nc_id(feed_id).unwrap(), new_title)
                .await
                .context(FeedApiErrorKind::Api)?;
            return Ok(feed_id.clone());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn add_category(&self, title: &str, _parent: Option<&CategoryID>, client: &Client) -> FeedApiResult<CategoryID> {
        if let Some(api) = &self.api {
            let folder = api.create_folder(client, title).await.context(FeedApiErrorKind::Api)?;
            return Ok(CategoryID::new(&folder.id.to_string()));
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn remove_category(&self, id: &CategoryID, _remove_children: bool, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api.delete_folder(client, Self::id_to_nc_id(id).unwrap())
                .await
                .context(FeedApiErrorKind::Api)?;
            return Ok(());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn rename_category(&self, id: &CategoryID, new_title: &str, client: &Client) -> FeedApiResult<CategoryID> {
        if let Some(api) = &self.api {
            api.rename_folder(client, Self::id_to_nc_id(id).unwrap(), new_title)
                .await
                .context(FeedApiErrorKind::Api)?;
            return Ok(id.clone());
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn import_opml(&self, _opml: &str, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn add_tag(&self, _title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn remove_tag(&self, _id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn rename_tag(&self, _id: &TagID, _new_title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn tag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn untag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn get_favicon(&self, _feed_id: &FeedID, _client: &Client) -> FeedApiResult<FavIcon> {
        Err(FeedApiErrorKind::Unsupported.into())
    }
}
