use super::config::AccountConfig;
use super::oauth::InoreaderOAuth;
use super::Inoreader;
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{LoginGUI, OAuthLoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, VectorIcon};
use chrono::{DateTime, NaiveDateTime, Utc};
use failure::Fail;
use failure::ResultExt;
use greader_api::{ApiError, ApiErrorKind, AuthData, GReaderApi, InoreaderAuth};
use parking_lot::RwLock;
use reqwest::Url;
use rust_embed::RustEmbed;
use std::path::PathBuf;
use std::str;
use std::sync::Arc;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/inoreader/icons"]
struct InoreaderResources;

pub struct InoreaderMetadata;

impl InoreaderMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("inoreader")
    }
}

impl ApiMetadata for InoreaderMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = InoreaderResources::get("feed-service-inoreader.svg").ok_or(FeedApiErrorKind::Resource)?;
        let icon = VectorIcon {
            data: icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = InoreaderResources::get("feed-service-inoreader-symbolic.svg").ok_or(FeedApiErrorKind::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let api_info = InoreaderOAuth::new();
        let login_url = Url::parse(&api_info.login_url()).unwrap();

        let login_gui = LoginGUI::OAuth(OAuthLoginGUI {
            login_website: Some(login_url),
            catch_redirect: Some(api_info.redirect_uri()),
        });

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("Inoreader"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: Some(crate::models::Url::parse("https://inoreader.com").unwrap()),
            service_type: ServiceType::Remote { self_hosted: false },
            license_type: ServiceLicense::GenericProprietary,
            service_price: ServicePrice::PaidPremimum,
            login_gui,
        })
    }

    fn parse_error(&self, error: &dyn Fail) -> Option<String> {
        if let Some(error) = <dyn Fail>::downcast_ref::<ApiError>(error) {
            if let ApiErrorKind::GReader(error) = error.kind() {
                return Some(error.errors.join("; "));
            }

            return Some(format!("{}", error));
        }
        None
    }

    fn get_instance(&self, path: &PathBuf, portal: Box<dyn Portal>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path).context(FeedApiErrorKind::Config)?;
        let mut api: Option<GReaderApi> = None;
        let mut logged_in: bool = false;

        if let Some(access_token) = account_config.get_access_token() {
            if let Some(refresh_token) = account_config.get_refresh_token() {
                if let Some(expires_at) = account_config.get_token_expires() {
                    let timestamp = expires_at.parse::<i64>().context(FeedApiErrorKind::Config)?;
                    let expires_at = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(timestamp, 0), Utc);
                    let api_info = InoreaderOAuth::new();

                    api = Some(GReaderApi::new(
                        &crate::models::Url::parse("https://inoreader.com").unwrap(),
                        AuthData::Inoreader(InoreaderAuth {
                            client_id: api_info.client_id.clone(),
                            client_secret: api_info.client_secret,
                            access_token,
                            refresh_token,
                            expires_at,
                        }),
                    ));
                    logged_in = true;
                }
            }
        }

        let inoreader = Inoreader {
            api,
            portal,
            logged_in,
            config: Arc::new(RwLock::new(account_config)),
        };
        let template = Box::new(inoreader);
        Ok(template)
    }
}
