mod config;
pub mod metadata;
mod oauth;

use std::collections::HashSet;
use std::sync::Arc;

use self::config::AccountConfig;
use self::metadata::InoreaderMetadata;
use self::oauth::InoreaderOAuth;
use crate::feed_api::{FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{
    self, ArticleID, Category, CategoryID, FavIcon, Feed, FeedID, Headline, LoginData, Marked, OAuthData, PluginCapabilities, Read, SyncResult,
    TagID, Url,
};
use crate::util::greader::{GReaderUtil, TAG_READ_STR, TAG_STARRED_STR};
use crate::{feed_parser, ParsedUrl};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use failure::ResultExt;
use greader_api::error::ApiErrorKind;
use greader_api::models::{AuthInput, InoreaderAuthInput, ItemId, StreamType};
use greader_api::{AuthData, GReaderApi};
use parking_lot::RwLock;
use reqwest::Client;

macro_rules! api_call {
    ( $sel:ident, $api:ident, $func:expr, $client:expr ) => {{
        let mut result = $func;
        if let Err(error) = &result {
            if error.kind() == ApiErrorKind::TokenExpired {
                let response = $api.inoreader_refresh_token($client).await.context(FeedApiErrorKind::Api)?;
                let token_expires = response.expires_at;
                $sel.config.write().set_access_token(&response.access_token);
                $sel.config.write().set_token_expires(&token_expires.timestamp().to_string());
                $sel.config.write().write().context(FeedApiErrorKind::Config)?;
                result = $func;
            }
        }
        result.context(FeedApiErrorKind::Api)?
    }};
}

pub struct Inoreader {
    api: Option<GReaderApi>,
    portal: Box<dyn Portal>,
    logged_in: bool,
    config: Arc<RwLock<AccountConfig>>,
}

impl Inoreader {
    async fn login_inoreader(&mut self, data: LoginData, client: &Client) -> FeedApiResult<()> {
        if let LoginData::OAuth(data) = data {
            let oauth = InoreaderOAuth::new();
            let url = Url::parse(&data.url).context(FeedApiErrorKind::Url)?;
            let auth_code = oauth.parse_redirected_url(&url)?;

            let mut api = GReaderApi::new(&*oauth.base_uri, AuthData::Uninitialized);
            let auth_data = api
                .login(
                    AuthInput::Inoreader(InoreaderAuthInput {
                        auth_code,
                        redirect_url: oauth.redirect_uri,
                        client_id: oauth.client_id,
                        client_secret: oauth.client_secret,
                    }),
                    client,
                )
                .await
                .context(FeedApiErrorKind::Login)?;

            let auth_data = match auth_data {
                AuthData::Inoreader(auth_data) => auth_data,
                _ => return Err(FeedApiErrorKind::Login.into()),
            };

            self.config.write().set_access_token(&auth_data.access_token);
            self.config.write().set_refresh_token(&auth_data.refresh_token);
            self.config.write().set_token_expires(&auth_data.expires_at.timestamp().to_string());

            let user = api.user_info(client).await.context(FeedApiErrorKind::Api)?;
            self.config.write().set_user_name(&user.user_name);
            self.config.write().write().context(FeedApiErrorKind::Config)?;
            self.api = Some(api);

            return Ok(());
        }

        Err(FeedApiErrorKind::Login.into())
    }
}

#[async_trait]
impl FeedApi for Inoreader {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::ADD_REMOVE_FEEDS
            | PluginCapabilities::SUPPORT_CATEGORIES
            | PluginCapabilities::MODIFY_CATEGORIES
            | PluginCapabilities::SUPPORT_TAGS)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(self.api.is_some())
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        Ok(self.logged_in)
    }

    fn user_name(&self) -> Option<String> {
        self.config.read().get_user_name()
    }

    fn get_login_data(&self) -> Option<LoginData> {
        if let Ok(true) = self.has_user_configured() {
            return Some(LoginData::OAuth(OAuthData {
                id: InoreaderMetadata::get_id(),
                url: String::new(),
            }));
        }

        None
    }

    async fn login(&mut self, data: LoginData, client: &Client) -> FeedApiResult<()> {
        if let Err(error) = self.login_inoreader(data, client).await {
            log::error!("Failed to log in: {}", error);
            self.api = None;
            self.logged_in = false;
            Err(FeedApiErrorKind::Login.into())
        } else {
            self.logged_in = true;
            Ok(())
        }
    }

    async fn logout(&mut self, _client: &Client) -> FeedApiResult<()> {
        self.config.read().delete()?;
        Ok(())
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let feeds = api_call!(self, api, api.subscription_list(client).await, client);
            let categories = GReaderUtil::convert_category_vec(feeds.subscriptions.clone());
            let tags = api_call!(self, api, api.tag_list(client).await, client);
            let tags = GReaderUtil::convert_tag_list(tags, &categories);
            let (feeds, mappings) = GReaderUtil::convert_feed_vec(feeds.subscriptions);

            let mut articles = Vec::new();
            let mut headlines = Vec::new();
            let mut taggings = Vec::new();

            let (mut unread_articles, mut unread_headlines, mut unread_taggings) = api_call!(
                self,
                api,
                GReaderUtil::get_articles(api, client, None, Some(Read::Unread), None, &tags, None, &self.portal).await,
                client
            );
            let (mut starred_articles, mut starred_headlines, mut starred_taggings) = api_call!(
                self,
                api,
                GReaderUtil::get_articles(api, client, None, None, Some(Marked::Marked), &tags, None, &self.portal).await,
                client
            );
            let (mut latest_articles, mut latest_headlines, mut latest_taggings) = api_call!(
                self,
                api,
                GReaderUtil::get_articles(api, client, None, None, None, &tags, Some(100), &self.portal).await,
                client
            );

            articles.append(&mut unread_articles);
            headlines.append(&mut unread_headlines);
            taggings.append(&mut unread_taggings);

            articles.append(&mut starred_articles);
            headlines.append(&mut starred_headlines);
            taggings.append(&mut starred_taggings);

            articles.append(&mut latest_articles);
            headlines.append(&mut latest_headlines);
            taggings.append(&mut latest_taggings);

            return Ok(SyncResult {
                feeds: crate::util::vec_to_option(feeds),
                categories: crate::util::vec_to_option(categories),
                mappings: crate::util::vec_to_option(mappings),
                tags: crate::util::vec_to_option(tags),
                taggings: crate::util::vec_to_option(taggings),
                headlines: None,
                articles: crate::util::vec_to_option(articles),
                enclosures: None,
            });
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn sync(&self, _max_count: u32, _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<SyncResult> {
        if let Some(api) = &self.api {
            let feeds = api_call!(self, api, api.subscription_list(client).await, client);
            let categories = GReaderUtil::convert_category_vec(feeds.subscriptions.clone());
            let tags = api_call!(self, api, api.tag_list(client).await, client);
            let tags = GReaderUtil::convert_tag_list(tags, &categories);
            let (feeds, mappings) = GReaderUtil::convert_feed_vec(feeds.subscriptions);

            let mut articles = Vec::new();
            let mut headlines = Vec::new();
            let mut taggings = Vec::new();

            // unread article ids
            let inoreader_unread_ids = api_call!(
                self,
                api,
                GReaderUtil::get_article_ids(api, client, None, Some(Read::Unread), None).await,
                client
            );
            let inoreader_unread_ids: HashSet<ArticleID> = inoreader_unread_ids
                .into_iter()
                .map(|item_id| {
                    let ItemId { id } = item_id;
                    ArticleID::from_owned(id)
                })
                .collect();

            // marked (saved/starred) article ids
            let inoreader_marked_ids = api_call!(
                self,
                api,
                GReaderUtil::get_article_ids(api, client, None, None, Some(Marked::Marked)).await,
                client
            );
            let inoreader_marked_ids: HashSet<ArticleID> = inoreader_marked_ids
                .into_iter()
                .map(|item_id| {
                    let ItemId { id } = item_id;
                    ArticleID::from_owned(id)
                })
                .collect();

            // get local unread
            let local_unread_ids = self.portal.get_article_ids_unread_all().context(FeedApiErrorKind::Portal)?;
            let local_unread_ids = local_unread_ids.into_iter().collect();

            // sync new unread articles
            let missing_unread_ids: HashSet<ArticleID> = inoreader_unread_ids.difference(&local_unread_ids).cloned().collect();
            if !missing_unread_ids.is_empty() {
                let (mut missing_unread_articles, mut missing_unread_taggings) = api_call!(
                    self,
                    api,
                    GReaderUtil::get_article_contents(api, client, &missing_unread_ids, &tags, &self.portal).await,
                    client
                );
                articles.append(&mut missing_unread_articles);
                taggings.append(&mut missing_unread_taggings);
            }

            // mark remotely read article as read
            let should_mark_read_ids: Vec<ArticleID> = local_unread_ids.difference(&inoreader_unread_ids).cloned().collect();
            let mut should_mark_read_headlines = should_mark_read_ids
                .into_iter()
                .map(|id| {
                    let marked = if inoreader_marked_ids.contains(&id) {
                        Marked::Marked
                    } else {
                        Marked::Unmarked
                    };

                    Headline {
                        article_id: id,
                        unread: Read::Read,
                        marked,
                    }
                })
                .collect();
            headlines.append(&mut should_mark_read_headlines);

            // get local marked
            let local_marked_ids = self.portal.get_article_ids_marked_all().context(FeedApiErrorKind::Portal)?;
            let local_marked_ids = local_marked_ids.into_iter().collect();

            // sync new marked articles
            let missing_marked_ids: HashSet<ArticleID> = inoreader_marked_ids.difference(&local_marked_ids).cloned().collect();
            if !missing_marked_ids.is_empty() {
                let (mut missing_marked_articles, mut missing_marked_taggings) = api_call!(
                    self,
                    api,
                    GReaderUtil::get_article_contents(api, client, &missing_marked_ids, &tags, &self.portal).await,
                    client
                );
                articles.append(&mut missing_marked_articles);
                taggings.append(&mut missing_marked_taggings);
            }

            // mark remotly starred articles locally
            let mut mark_headlines = inoreader_marked_ids
                .iter()
                .map(|id| Headline {
                    article_id: id.clone(),
                    marked: Marked::Marked,
                    unread: if inoreader_unread_ids.contains(id) { Read::Unread } else { Read::Read },
                })
                .collect();
            headlines.append(&mut mark_headlines);

            // unmark remotly unstarred articles locally
            let missing_unmarked_ids: Vec<ArticleID> = local_marked_ids.difference(&inoreader_marked_ids).cloned().collect();
            let mut missing_unmarked_headlines = missing_unmarked_ids
                .into_iter()
                .map(|id| {
                    let unread = if inoreader_unread_ids.contains(&id) { Read::Unread } else { Read::Read };

                    Headline {
                        article_id: id,
                        marked: Marked::Unmarked,
                        unread,
                    }
                })
                .collect();
            headlines.append(&mut missing_unmarked_headlines);

            // get some of the latest articles regardless of state
            let (mut latest_articles, mut latest_headlines, mut latest_taggings) = api_call!(
                self,
                api,
                GReaderUtil::get_articles(api, client, None, None, None, &tags, Some(50), &self.portal).await,
                client
            );
            articles.append(&mut latest_articles);
            headlines.append(&mut latest_headlines);
            taggings.append(&mut latest_taggings);

            return Ok(SyncResult {
                feeds: crate::util::vec_to_option(feeds),
                categories: crate::util::vec_to_option(categories),
                mappings: crate::util::vec_to_option(mappings),
                tags: crate::util::vec_to_option(tags),
                taggings: crate::util::vec_to_option(taggings),
                headlines: crate::util::vec_to_option(headlines),
                articles: crate::util::vec_to_option(articles),
                enclosures: None,
            });
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn set_article_read(&self, articles: &[ArticleID], read: models::Read, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let item_ids: Vec<_> = articles.iter().map(|id| id.to_str()).collect();
            let (add_tag, remove_tag) = match read {
                Read::Read => (Some(TAG_READ_STR), None),
                Read::Unread => (None, Some(TAG_READ_STR)),
            };
            api_call!(self, api, api.tag_edit(&item_ids, add_tag, remove_tag, client).await, client);
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn set_article_marked(&self, articles: &[ArticleID], marked: models::Marked, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let item_ids: Vec<_> = articles.iter().map(|id| id.to_str()).collect();
            let (add_tag, remove_tag) = match marked {
                Marked::Marked => (Some(TAG_STARRED_STR), None),
                Marked::Unmarked => (None, Some(TAG_STARRED_STR)),
            };
            api_call!(self, api, api.tag_edit(&item_ids, add_tag, remove_tag, client).await, client);
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn set_feed_read(&self, feeds: &[FeedID], _articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for feed in feeds {
                api_call!(self, api, api.mark_all_as_read(feed.to_str(), None, client).await, client);
            }
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn set_category_read(
        &self,
        categories: &[CategoryID],
        _articles: &[ArticleID],
        _last_sync: DateTime<Utc>,
        client: &Client,
    ) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for category in categories {
                api_call!(self, api, api.mark_all_as_read(category.to_str(), None, client).await, client);
            }
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn set_tag_read(&self, tags: &[TagID], _articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            for tag in tags {
                api_call!(self, api, api.mark_all_as_read(tag.to_str(), None, client).await, client);
            }
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn set_all_read(&self, articles: &[ArticleID], _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<()> {
        self.set_article_read(articles, Read::Read, client).await
    }

    async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
        if let Some(api) = &self.api {
            let feed_id = format!("feed/{}", &url.as_str());
            let category_str = category_id.clone().map(|id| id.to_str().to_owned());

            api_call!(
                self,
                api,
                api.subscription_create(&*url, title.as_deref(), category_str.as_deref(), client).await,
                client
            );

            let feed_id = FeedID::new(&feed_id);
            let feed = match feed_parser::download_and_parse_feed(url, &feed_id, title, None, client).await {
                Ok(ParsedUrl::SingleFeed(feed)) => feed,
                _ => {
                    // parsing went wrong -> remove feed from inoreader account and return the error
                    self.remove_feed(&feed_id, client).await?;
                    return Err(FeedApiErrorKind::ParseFeed.into());
                }
            };

            // return category in case a new one got created
            let local_categories = self.portal.get_categories().context(FeedApiErrorKind::Portal)?;
            let category = if !local_categories.iter().any(|c| Some(&c.category_id) == category_id.as_ref()) {
                let feeds = api_call!(self, api, api.subscription_list(client).await, client);
                let categories = GReaderUtil::convert_category_vec(feeds.subscriptions);
                categories.iter().find(|c| Some(&c.category_id) == category_id.as_ref()).cloned()
            } else {
                None
            };

            return Ok((feed, category));
        }
        Err(FeedApiErrorKind::Login.into())
    }

    async fn remove_feed(&self, id: &FeedID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            let feed_id_str = id.to_str();
            api_call!(self, api, api.subscription_delete(feed_id_str, client).await, client);
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn move_feed(&self, feed_id: &FeedID, from: &CategoryID, to: &CategoryID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api_call!(
                self,
                api,
                api.subscription_edit(feed_id.to_str(), None, Some(from.to_str()), Some(to.to_str()), client)
                    .await,
                client
            );
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn rename_feed(&self, feed_id: &FeedID, new_title: &str, client: &Client) -> FeedApiResult<FeedID> {
        if let Some(api) = &self.api {
            api_call!(
                self,
                api,
                api.subscription_edit(feed_id.to_str(), Some(new_title), None, None, client).await,
                client
            );
            Ok(feed_id.clone())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn add_category(&self, title: &str, parent: Option<&CategoryID>, _client: &Client) -> FeedApiResult<CategoryID> {
        if let Some(_api) = &self.api {
            if parent.is_some() {
                return Err(FeedApiErrorKind::Unsupported.into());
            }

            // only generate id
            // useing id as if it would exist will create category
            let category_id = GReaderUtil::generate_tag_id(title);
            Ok(CategoryID::new(&category_id))
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn remove_category(&self, id: &CategoryID, remove_children: bool, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            if remove_children {
                let mappings = self.portal.get_mappings().context(FeedApiErrorKind::Portal)?;

                let feed_ids = mappings
                    .iter()
                    .filter(|m| &m.category_id == id)
                    .map(|m| m.feed_id.clone())
                    .collect::<Vec<FeedID>>();

                for feed_id in feed_ids {
                    self.remove_feed(&feed_id, client).await?;
                }
            }

            api_call!(self, api, api.tag_delete(StreamType::Category, id.to_str(), client).await, client);
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn rename_category(&self, id: &CategoryID, new_title: &str, client: &Client) -> FeedApiResult<CategoryID> {
        if let Some(api) = &self.api {
            api_call!(
                self,
                api,
                api.tag_rename(StreamType::Category, id.to_str(), new_title, client).await,
                client
            );
            Ok(CategoryID::new(&GReaderUtil::generate_tag_id(new_title)))
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn import_opml(&self, opml: &str, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api_call!(self, api, api.import(opml.to_owned(), client).await, client);
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn add_tag(&self, title: &str, _client: &Client) -> FeedApiResult<TagID> {
        if let Some(_api) = &self.api {
            // only generate id
            // useing id as if it would exist will create tag
            let tag_id = GReaderUtil::generate_tag_id(title);
            Ok(TagID::new(&tag_id))
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn remove_tag(&self, id: &TagID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api_call!(self, api, api.tag_delete(StreamType::Stream, id.to_str(), client).await, client);
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn rename_tag(&self, id: &TagID, new_title: &str, client: &Client) -> FeedApiResult<TagID> {
        if let Some(api) = &self.api {
            api_call!(
                self,
                api,
                api.tag_rename(StreamType::Stream, id.to_str(), new_title, client).await,
                client
            );
            Ok(TagID::new(&GReaderUtil::generate_tag_id(new_title)))
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn tag_article(&self, article_id: &ArticleID, tag_id: &TagID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api_call!(
                self,
                api,
                api.tag_edit(&[article_id.to_str()], Some(tag_id.to_str()), None, client).await,
                client
            );
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn untag_article(&self, article_id: &ArticleID, tag_id: &TagID, client: &Client) -> FeedApiResult<()> {
        if let Some(api) = &self.api {
            api_call!(
                self,
                api,
                api.tag_edit(&[article_id.to_str()], None, Some(tag_id.to_str()), client).await,
                client
            );
            Ok(())
        } else {
            Err(FeedApiErrorKind::Login.into())
        }
    }

    async fn get_favicon(&self, _feed_id: &FeedID, _client: &Client) -> FeedApiResult<FavIcon> {
        Err(FeedApiErrorKind::Unsupported.into())
    }
}
